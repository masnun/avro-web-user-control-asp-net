﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Avro.ascx.cs" Inherits="Avro" %>
<asp:Panel ID="avrojs" runat="server">
    <script type="text/javascript">
    
        var root = (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]);
        var ns = document.createElementNS && document.documentElement.namespaceURI;

        if (typeof jQuery === 'undefined') {
            var script = ns ? document.createElementNS(ns, 'script') : document.createElement('script');
            script.type = 'text/javascript';
            script.onreadystatechange = function () {
                if (this.readyState == 'complete') enable_avro();
            }
            script.onload = enable_avro;
            script.src = 'https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js';
            root.appendChild(script);
        }
        else {
            enable_avro();
        }

        function enable_avro() {
            jQuery.noConflict();
            var script = ns ? document.createElementNS(ns, 'script') : document.createElement('script');
            script.type = 'text/javascript';
            script.onreadystatechange = function () {
                if (this.readyState == 'complete') avro_js_loader();
            }
            script.onload = avro_js_loader;
            script.src = 'https://raw.github.com/torifat/jsAvroPhonetic/master/dist/avro-latest.js';
            root.appendChild(script);

            preload_avro_images();

        }

        function avro_js_loader() {
            jQuery(function () {
                jQuery('textarea, input[type=text]').avro({ 'bn': <asp:Literal ID="bn" runat="server" /> }, function (isBangla) {
                   <asp:Literal ID="callback" runat="server" />(isBangla);
                });

            });
        }


        function preload_avro_images() {

            var avro_preload = ['https://github.com/masnun/Avro-Phonetic-WP-Plugin/raw/master/avro-bangla.png', 'https://github.com/masnun/Avro-Phonetic-WP-Plugin/raw/master/avro-english.png'];
            var avro_images = [];

            for (var i = 0; i < avro_preload.length; i++) {
                avro_images[i] = new Image();
                avro_images[i].src = avro_preload[i];
            }
        }
    </script>
</asp:Panel>
