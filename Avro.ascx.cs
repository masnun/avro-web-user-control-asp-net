﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Avro : System.Web.UI.UserControl
{
    public string Bangla
    {
        set
        {
            bn.Text = value;
        }

        get
        {
            return bn.Text;
        }

    }

    public string Callback
    {
        set
        {
            callback.Text = value;
        }

        get
        {
            return callback.Text;
        }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(this.Bangla))
        {
            this.Bangla = "true";
        }
    }
}